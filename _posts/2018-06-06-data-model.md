---
title: Sea Lion Data Model
author: Mike
layout: post
---
At the beginning of this project I had zero knowledge of Firebase and had never used a document database before.

Before deciding on Firebase as our platform for our Sea Lion database, we all did research on it. We decided it would be easy to implement and has features that would be beneficial to our client.

We liked the idea that Firebase will host the site and enables offline capabilities for Android.

As a team our first step was to design our initial database model. We looked into how to design a document database, and came up with this model:

![My helpful screenshot]({{ "/assets/images/initial_model.png" | absolute_url }})

I set up our initial Firebase app following this tutorial: 

[Firebase Tutorial](https://firebase.google.com/docs/web/setup)

After learning how to connect to firebase, I created this basic function that takes data from a form and creates a Sea Lion document in Firebase.

{% highlight ruby %}
function addSeaLion()
{
    //get ref to database
        var data = firebase.database();

    //gets everything inside sealion
    var sealion = data.ref().child("sealion"); 
    
    //Add new sealion. Store unique id into key
    var key = sealion.push(
    {
        details: {
        name: document.getElementById("name").value,
        mother: document.getElementById("mother").value,
        dob: document.getElementById("dob").value,
        pob:  document.getElementById("pob").value,
        gender: document.getElementById("gender").value,
        transponder: document.getElementById("transponder").value
        },
        tags: {
                date_in: document.getElementById("tagdate").value,
                type: document.getElementById("tagtype").value,
                description: document.getElementById("tagdescription").value,
                number: document.getElementById("tagnumber").value,
                rf_number: document.getElementById("rfnumber").value,
                left_attached: "Yes",
                right_attached: "Yes"
        },
        toes: {

                clipped_left: getClippedArray("left[]"),
                clipped_right: getClippedArray("right[]")
        },
    });

{% endhighlight %}

Throughout the project our database model changed several times.

The problem was that a Sea Lion can have multiple features, and each feature can have multiple images.

I came up with this crude solution. Since it is a document database, any document can hold any number of other documents.


![My helpful screenshot]({{ "/assets/images/sea_lion_firebase.png" }})

This is how the sea lion document looks in the Firebase console, which was very helpful.

Documents can't store images, but Firebase can store them. So I just needed to store how many images they had, and use this to get the images later.

At this point is when I started to doubt how good firebase would be. I wanted to get a simple count of the amount of features each sea lion had.

But there was no simple query to do this, so I ended up storing it as a field in the sea lion features document.

This was all working well, then we started our filtering of sea lions. We realised the Firebase Real-time database was very limited.

We had to change the Firebase Firestore, which still wasn't the best, but better the better option in regards to filtering.

I ended up refactoring our data model to look this this:

![My helpful screenshot]({{ "/assets/images/firestore_sealion.png" }})

![My helpful screenshot]({{ "/assets/images/firestore_feature.png" }})

This is the final data model of our Sea Lions, being stored in the Firestore document database. This ended up being the best data model I think.

It made filtering much easier. Instead of have a feature inside the sea lion, the features now reference the sea lions unique id, which simulates a foreign key.

<h2>Reflections</h2>
<p></p>
I have learned to not rush the design of the data model. I ending up having to refactor our create sea lion code 3 or 4 times throughout the project. 
If we had thought out our initial model better, it might have saved a lot of time. The model was very simple, but it ended up being completed to implement because of the firebase limitations.

Next time I wouldn't use a document database. We are missing the referential integrity that come with SQL, and also the querying capabilities.






