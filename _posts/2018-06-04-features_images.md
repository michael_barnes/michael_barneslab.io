---
title: Features and Images
author: Mike
layout: post
---
At the beginning of this project I had limited knowledge in JavaScript.
Working out how to add features and images has taught me a lot.
The problem was that each Sea Lion could have multiple features, and each feature could have many images.
We couldn't put a limit on this, because features is the field that is most likely going to get updated.

The first issue was - How do let the user make as many features as they want without limiting them.

My solution was to have an Add Features button.

![My helpful screenshot]({{ "/assets/images/add_feature.png" | absolute_url }})

The user could create as many features as they wanted now. This button would create a description text field, and an file input button.

I knew this had to be implemented in JavaScript, because it happens on a click event. So I researched how to create DOMs in JavaScript.

W3schools had a good [tutorial](https://www.w3schools.com/jsref/met_document_createelement.asp) on this.

I coded my first dynamically created form:

{% highlight ruby %}
function createFeatures()
{  
    
    featureCount++;

    var form = document.createElement('form');    
    var field = document.createElement('fieldset');
    var label = document.createElement('label');
    var textarea = document.createElement('textarea');          
    var input=document.createElement('input');
    var div = document.getElementById("features");

    label.htmlFor = "featuredescription"+featureCount;
    label.innerHTML = "Description: ";
    form.className= "pure-form pure-form-stacked";
    textarea.id = "featuredescription"+featureCount; 
    input.type="file";
    input.multiple = true;
    input.id = "files"+featureCount+"[]";

    form.appendChild(field);
    field.appendChild(label);
    field.appendChild(textarea);
    field.appendChild(input);    
    div.appendChild(form);    
}
{% endhighlight %}

All I needed now was the logic behind adding images.

I learned the basics of uploading images from [Firebase upload image documentation](https://firebase.google.com/docs/storage/web/upload-files).
 
My initial code wasn't very elegant but I got it working. For each sea lion, we had to create an features object holding an array of features.
Using the feature count that is incremented when addFeature is called, I knew how many features to add.
I then had to store the image in a directory structure like this - sealion/<sealion_id>/features/feature<featureIndex>/image
I just took the size of each file array to find out how many images each feature had.

{% highlight ruby %}
 function getFeatures()
{
    features = {size : featureCount, feature : []};
    //Only add image if they have added a feature
    if (featureCount > 0)
    {
        //Loop through all the features
        for (var featuresIndex = 0; featuresIndex <= featureCount;  featuresIndex++)
        {
            var descriptionElement = "featuredescription"+featuresIndex;                     
            features.feature.push({description : document.getElementById(descriptionElement).value, images : 0});            
        }
    }

    return features;
}

//uploads images and stores them under sea lions unique id
function uploadImage(id, currentFeature)
{
    //Only add image if they have added a feature
    if (featureCount > 0)
    {
        //Loop through all the features
        for (var featuresIndex = 0; featuresIndex <= featureCount;  featuresIndex++)
        {
            //Get the name of the array holding files
            var filesarray = "files"+featuresIndex+"[]";
            // Create a root reference
            var files = document.getElementById(filesarray).files; // use the Blob or File API
            
            //Only try and upload image if the files array has something in it
            if (files.length > 0)
            {
                //Update image length of data
                var refString = "sealion/"+id+"/features/feature/"+featuresIndex;
                var sealion = firebase.database().ref(refString);                
                sealion.update({ images: files.length });

                    //Loop through each image in files and add to firebase storage
                for (var imageIndex = 0; imageIndex < files.length; imageIndex++)
                {
                    var storageRef = firebase.storage().ref('features/' + id + "/feature" + featuresIndex + "/image" + imageIndex); 
                    var task = storageRef.put(files[imageIndex]);                   
                }
            }                     
        }
    }
}
{% endhighlight %}
 
 After changing from Firebase to Firestore, our data model changed. So I had to refactor the code to make it work. It ending up making the
 code simpler. Now each feature has a link to a Sea Lion, so we only need to store which feature the image is linked to.
 The directory structure is much simpler - <feature_id>/image
 
{% highlight ruby %}
function addFeatures(id)
{    
    //Only add image if they have added a feature
    if (featureCount > 0)
    {        
        //Loop through all the features
        for (var featuresIndex = 0; featuresIndex <= featureCount;  featuresIndex++)
        {            
            var descriptionElement = "featuredescription"+featuresIndex;                     
            feature_object = {description : document.getElementById(descriptionElement).value, images : 0, id : id};   

            db.collection("Feature").add(feature_object).then(function(feature)
            {
                console.log("Document written with ID: ", feature.id);                
                uploadImage(feature.id);

            }).catch(function(error) 
            {
                console.error("Error adding document: ", error);
            });          
        }
    }    
}

//uploads images and stores them under sea lions unique id
function uploadImage(id)
{        
    //Get the name of the array holding files
    var filesarray = "files"+fileArrayIndex+"[]";

    console.log(filesarray);
    // Create a root reference
    var files = document.getElementById(filesarray).files; // use the Blob or File API
    console.log(files);
    
    //Only try and upload image if the files array has something in it
    if (files.length > 0)
    { 
        var featureRef = db.collection("Feature").doc(id);            
        featureRef.update({
            images: files.length
        });

        //Loop through each image in files and add to firebase storage
        for (var imageIndex = 0; imageIndex < files.length; imageIndex++)
        {
            var storageRef = firebase.storage().ref(id + "/image" + imageIndex); 
            var task = storageRef.put(files[imageIndex]);           
        }
    }     
    fileArrayIndex++;  
}


{% endhighlight %}


<h2>Reflections</h2>
<p></p>
Adding features and images taught me some JavaScript basics. I got the hang of anonymous functions, JavaScript objects, and I learned how to dynamically create DOMs. 

If I were to do this again I would have tried to do this using a JavaScript framework like Angular or React. However, I think it value to do it with JavaScript first and
get a foundation knowledge of it. Using a framework would help with the structuring of my JavaScript code, because the tutorials I found didn't really mention this.






