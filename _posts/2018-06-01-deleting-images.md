---
title: Deleting Images
author: Mike
layout: post
---
When a Sea Lion is deleted form the database, all of the features that belong to the Sea Lion have to be deleted as well.
This means all of the images linked to each feature had to be deleted as well. If this didn't happen we would run out of storage space.

The directory structure for storing images is simple. There is a folder for each feature, and that folder can have x amount of images in it.
I thought deleting images would be very simple. All I had to do was delete the folder containing the images.

Unfortunately Firebase doesn’t support this. Each image has to be deleted individually, and when the directory is empty, it deletes itself.

This was annoying, but I think I came up with a good solution.

My logic was:

-	Get the id of feature being deleted
- 	Delete the feature
- 	Use id to create pathway to image
- 	Keep deleting images until no image error is caught

{% highlight ruby %}
function deleteFeatures()
{    
    checkboxes = document.getElementsByName("delete_checkbox");    
    
    for (let i = 0; i < checkboxes.length; i++)
    {
        if (checkboxes[i].checked)
        {             
            id = checkboxes[i].id.substring(6);            
            ref = db.collection("Feature").doc(id);
            ref.delete().then(function()
            {                
                console.log("Document deleted");                
            }).catch(function(error) 
            {
                console.error("Error adding document: ", error);
            });

            deleteImages(id);
        }
    }    
}

function deleteImages(id)
{   
    let hasImages = true;
    let i = 0;
    
    while (hasImages)
    {        
        path = id+"/"+"image"+i;        
        let storageRef = firebase.storage().ref(path);  
		storageRef.delete().then(function()
		{
		// File deleted successfully
			
			console.log(id+ " deleted");
		}).catch(function(error) {
			hasImages = false;
			console.log(error);
		});    
		i++;
	}
}

{% endhighlight %}

It was not as simple as I thought though. Because Firebase calls happen asynchronously, the loop was endless, and it would crash the browser.

I knew about promises at this point, but I hadn't fully got my head around them.

I tried adding promises to the loop, but I didn't know what I was doing and it didn't help at all.


{% highlight ruby %}
function deleteImage(id)
{
    let promises = []
    let hasImages = true;
    let i = 0;
    
    while (hasImages)
    {        
        path = id+"/"+"image"+i;        
        let storageRef = firebase.storage().ref(path); 
        console.log(path);
        promises.push
        (
            storageRef.delete().then(function()
            {
            // File deleted successfully
                i++;
                console.log(id+ " deleted");
            }).catch(function(error) {
                hasImages = false;
                console.log(error);
            })            
        );
        Promise.all(promises);
    }
}
{% endhighlight %}

[Then I discovered async await](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await).

{% highlight ruby %}
async function deleteImage(id)
{
    let hasImages = true;
    let i = 0;

    while (hasImages == true)
    { 
        path = id+"/"+"image"+i;        
        let storageRef = firebase.storage().ref(path); 
               
        await storageRef.delete().then(function()
        {
        // File deleted successfully                
            console.log(id+ " deleted");

        }).catch(function(error) { 
            hasImages = false;
        })

        if (i == 100)
        {
            hasImages = false;
        }
        i++;
    }    
}
{% endhighlight %}

This made the loop wait for the image to be deleted before continuing, which made it work perfectly.

I added an extra check to stop the endless loop in case something goes wrong.


<h2>Reflections</h2>
<p></p>
I was proud that I got it working. It was a bit over my head dealing with asynchronous calls.
After spending more time dealing with them, I learnt how to correctly handle promises. I will talk about this in another post.
Next time I wouldn't do it this way, because using Promises.all() is better.

I would also find the amount of images form features before I deleted it, and not use catching an error as a condition for my loop.
This has it's problems, but I feel it is more elegant.

Deleting images helped my understand asynchronous calls more, and got me on track to understanding promises.















