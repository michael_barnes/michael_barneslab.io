---
title: Unit Testing
author: Mike
layout: post
---
Unit testing is important in every project. Firebase only had and testing SDK for Cloud Functions, which we didn't use.
I researched potential JavaScript testing frameworks, and found Jasmine to be a good option.

I chose Jasmine because it has low overhead and no external dependencies.

I didn't know how to get started, so I followed this amazing [tutorial.](https://jasmine.github.io/2.1/introduction)

Jasmine is very simple, it just uses functions, the main ones being describe, it, and expect.

{% highlight ruby %}
function createFeatures()


describe("A suite is just a function", function() {
  var a;

  it("and so is a spec", function() {
    a = true;

    expect(a).toBe(true);
  });
});


{% endhighlight %}

The concept was simple to understand but I ran into the problem of actually testing our code.

The problem was our code never returns anything. It takes data and adds data to the database, and updates the DOMs. I didn't know how to test this.

I found this [tutorial](https://dev.to/aurelkurtula/unit-testing-with-jasmine-the-very-basics-74k) that had a solution to my problem.

The tutorial says to generate a mock form with JavaScript, and use it for testing.

{% highlight ruby %}

describe('Adding sea lion', function()
{     
    var test_sealion; 

    beforeEach(function(done) 
    {
        const form = document.createElement('form');
        form.id = "form";        
        form.innerHTML= 
        `<input id="name" value="Testing" />
        <input id="mother" value="Testing" />
        <input id="dob" value="Testing" />
        <input id="pob" value="Testing" />
        <input id="gender" value="Testing" />
        <input id="transponder" value="Testing" />
        <input id="living_status" value="Testing" />
        <input id="tagdatein" value="Testing" />
        <input id="lefttagdateout" value="Testing" />
        <input id="righttagdateout" value="Testing" />
        <input id="tagtype" value="Testing" />
        <input id="tagcolour" value="Testing" />
        <input id="tagnumber" value="Testing" />
        <input id="rfnumber" value="Testing" />        
        `;

        for (var i = 0; i < 5; i++)
        {
            var left = document.createElement("INPUT");
            left.setAttribute("type", "checkbox"); 
            left.name = "left[]";
            left.checked = true;
            var right = document.createElement("INPUT");
            right.setAttribute("type", "checkbox"); 
            right.name = "right[]";
            right.checked = true;
            form.appendChild(left);
            form.appendChild(right);                
        }  
        
        var div = document.createElement("div");
        div.id = "displaySealions";        
        document.body.appendChild(form);  
        document.body.appendChild(div);  
        document.getElementById("form").style.visibility = "hidden";
        
        addSeaLion();        

        db.collection("Sea Lions").where("name", "==", "Testing").get().then(function(document) 
        {        
            document.forEach(function(sealion)
            {
                test_sealion = sealion.data();

                done();
                
                db.collection("Sea Lions").doc(sealion.id).delete().then(function() {
                    console.log("Document successfully deleted!");
                }).catch(function(error) {
                    console.error("Error removing document: ", error);
                }); 
            });            
        }); 
    });
    
    it('Sea lion variable created', function(done)
    {
        expect(test_sealion.name).toBe("Testing");
        done();        
    });

});


{% endhighlight %}

<h2>Reflections</h2>
<p></p>
I wasn't happy with this solution, but I got the test working. I thought having to create all the data in a hidden form seemed wrong. The form had to be hidden because the Jasmine interface
is an HTML page, and adding a visible form to it would have broken it.

Next time I would use a library called jasmine-jquery. It uses HTML fixtures to load HTML content to be used by tests. It looks like a lot less work, but I was reluctant to try it because it uses j-query.
I wish I had put in the time learn j-query, I think it would have made our entire application much simpler.

Next time I will defiantly use j-query and jasmine-jquery for testing.

This test only tests the addSeaLion method. To actually test if the data was in the database I had to write a search method to look for it. When I created the testing we have no search functionality so 
this was the only way to do it. I should have tried to get 100% coverage, and next time I will make this a priority.

This test was also putting data into our development database. I should have created a separate Firebase account designed for running tests on.

I now know the basics of Jasmine, and I understand how JavaScript testing works. I think I approached most of my testing the wrong way. But struggling through it has helped my learn
why I was wrong, and I can use this knowledge to do a better job next time.







