---
title: Permissions and Hosting
author: Mike
layout: post
---
One of the reasons we chose Firebase is because it hosts static content for you. This meant our client wouldn’t ask have to 
pay for a server.

I had never deployed a website before, and I feel I did things the wrong way around. 
I followed these [instructions](https://firebase.google.com/docs/hosting/quickstart) to get hosting up and running.

I had to use npm to install Firebase tools and initialise our directory that had our app inside it. I was very confused at this point. I thought this was the only place we could work on the app, and that to test it I had to deploy it every time.

I didn't know how to share my work with my team members, because they wouldn't be able to deploy the app when they made changes. I learned that this is a bad way to approach building an app. putting it on a live server is that last thing that should happen.

Learning Jekyll and Django in other classes helped me understand how to set up a development server. I was then able to test the application anywhere I went, by storing it on Gitlab and running it on a local server.

Another problem that was holding me back was Firebase permissions. The default storage rules are that only authenticated users and upload and download images, and this is what I wanted to test.

The script below sets up the Firebase Authentication built in login.

{% highlight ruby %}
// FirebaseUI config.
var uiConfig = {
    signInSuccessUrl: '/add_sealion.html',
    signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.          
        firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    // Terms of service url.
    tosUrl: '<your-tos-url>'
};

{% endhighlight %}

I was very happy how simple it was to do. I just had to create an index.html page that links this script, and Firebase did the rest.

I decided the server we had our app on would become our development server, and later on we would create another Firebase account and use that as our production server.

Later on in the project I learned the Firebase authentication wasn't as great as it initially seemed. It lets you sign in and out, but it doesn't let you define what users can and can't do.

I did some research and found the only solution is to manage the users yourself. 

This meant creating another document in the database and keep track of their permissions.

I created a simple form that creates a user, and choose their permissions.

![]({{ "/assets/images/admin.png" }})

Firebase had built in methods for creating users, and sending password reset. I had write a method that takes the added user id and stores it in the database, along with
different types of permissions.

{% highlight ruby %}

function addUser()
{
    var email = document.getElementById("email");
    var password = document.getElementById("password");

    console.log(email);
    console.log(password);

    firebase.auth().createUserWithEmailAndPassword(email.value, password.value).then(function(user)
	{
		addUserPermissions(user.uid);
		alert("New user added");
		sendPasswordReset();			
		
	}).catch(function(error)
	{
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
		console.log(error.message);
		
		if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }
        console.log(error);		
    });
}

function addUserPermissions(userID)
{
	var read = document.getElementById("read").checked;
	var write = document.getElementById("write").checked;
	var update = document.getElementById("update").checked;
	var del = document.getElementById("delete").checked;	
	
	db.collection("Users").doc(userID).set({
    read: read,
    write: write,
    update: update,
	delete: del
	})
	.then(function() {
		console.log("Document successfully written!");
	})
	.catch(function(error) {
		console.error("Error writing document: ", error);
	}); 
}


function sendPasswordReset() 
{
      var email = document.getElementById('email').value;
      // [START sendpasswordemail]
      firebase.auth().sendPasswordResetEmail(email).then(function() {
        // Password Reset Email Sent!
        // [START_EXCLUDE]
        alert('Password Reset Email Sent!');
		document.getElementById("admin_form").reset();
        // [END_EXCLUDE]
      }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // [START_EXCLUDE]
        if (errorCode == 'auth/invalid-email') {
          alert(errorMessage);
        } else if (errorCode == 'auth/user-not-found') {
          alert(errorMessage);
        }
		
        console.log(error);
        // [END_EXCLUDE]
      });
      // [END sendpasswordemail];
}

{% endhighlight %}

This ending up create a document in our database that looked like this:

![]({{ "/assets/images/users.png" }})

Unfortunately other user stories became a priority and I didn't have time use the permissions on the rest of the app.

<h2>Reflections</h2>
<p></p>
I started off with the prototype version of the app being hosted on a live server. This wasn't a good idea, but it was the only way I knew how to get things working.
Next time I would start off with a local development server, which I now know how to do.

I was impressed by Firebase's authentication system at first, but came to realise it didn't have all of the features I needed.

This problem was caused by lack of research. I saw Firebase had authentication and started with the basics and decided it was great. I didn't think about the big picture.

Next time I would try and use a Javascript authentication framework like Passport or Nest. 








