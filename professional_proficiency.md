---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Professional Proficiency
order: 5
---

<p>In this section of the portfolio I will discuss my Professional Proficiency during the project.</p>

<p>The main topics I will be talking about are:</p>

<ul>
	<li>Communication</li>
	<li>Leadership</li>
	<li>Team work</li>
	<li>Time management</li>
		
</ul>
<p></p>
I will discuss about my thoughts, reflections, and what I have learned from these topics.

