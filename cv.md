---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Curriculum vitae
subtitle: Michael Barnes
order: 3
---
<object data="/assets/michael_barnes_cv.pdf" width="1000" height="2400" type='application/pdf'/>



