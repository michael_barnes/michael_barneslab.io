---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Technical Report
subtitle: Michael Barnes
order: 2
---
<object data="/assets/technical_report.pdf" width="1000" height="1200" type='application/pdf'/>



