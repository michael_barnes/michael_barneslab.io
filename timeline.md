---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Timeline
order: 1
---

<h3>Week One</h3><br>
Formed our group. Had a rough idea of project. Brainstormed potential technologies we could use to build database.
We were given our client's contact information and given a brief outline of our project.
We made a group email and emailed our client to set up a face to face meeting to learn about the project.

We set up Slack channel to use for communicating but planned to communicate face to face whenever possible. 
Set up a GitHub repo which we would use for version control and managing user stories.


<h3>Week Two</h3><br>
Started preparing for meeting with client. Had a group meeting and worked out the questions we would ask. 
We wanted to get a full idea of the problem, so we could create good user stories.

Meeting went very well. Jordana was friendly and very passionate about Sea Lions. We got a good idea of the problem. We discussed what needed to happen. In short, an online relational database to keep track of Sea Lions in Otago. The current data is stored in multiple excel spreadsheets and is difficult to maintain.

<u>Started sprint – Research technologies that would be suitable for the Sea Lion database. </u>

Aspects to consider when researching:

- Data format is relatively simple
- Needs free hosting
- Compatible for mobile phone if the future
- Offline syncing capabilities for mobile


<h3>Week Three</h3><br>
Possible technologies - CouchDB, Firebase.

Thinking about NoSql and the type of database. Learning a new technology is the point of this.

Picked firebase because of offline syncing, free hosting, and it's a NoSql database which will be useful to learn.

<u>Started sprint – Design Data model, get basic understanding of fire base</u>

I started thinking about the design of the data model. I looked into how document database design works.

I watched videos on Firebase and had look at the documentation.

<h3>Week Four</h3><br>
<u>Sprint 2 – Create Database in Firebase, Adding a sea lion

I followed the firebase docs and connected an html page to firebase. 

Set up a test database in Firebase real-time database.
Managed to add data to the document. Got the hang of the Firebase console.

<h3>Week Five</h3><br>
<u>Sprint 2 – Create Database in Firebase, Adding a sea lion</u>

Worked on adding a Sea Lion. Create basic form. Took data from form to create Sea Lion object based on data model.
Got form working with JavaScript which adds Sea Lion to database. Started working on adding features and images. 
Got authentication and hosting working.

<h3>Week Six</h3><br>
<u>Sprint 3 – Design and Filtering, Create Database in Firestore</u>

Changed from Real Time Database to Firestore database. Had to redesign data model.
Refactored adding code. Got features and images adding.


<h3>Week Seven</h3><br>
<u>Sprint 3 – Design and filter</u>

Started Unit testing in Jasmine. Re-added project to development server.
 

<h3>Week Eight</h3><br>
<u>Sprint 3 – Design and filter, Add new location and Colour</u>

Helped Connor with adding dynamic select boxes. Finished unit testing.
Started creating admin page.

<h3>Week Nine</h3><br>
<u>Sprint 4 – Update and Delete, adding data, Displaying Data</u>

Started update and delete. Help Fawaz with getting Sea Lion data to display in modal.

<h3>Week Ten</h3><br>
<u>Sprint 4 – Update and Delete, Displaying Data, Adding data</u>

Sea Lions can be updated. Started working on images and features.
Started working on deleting features and images.

<h3>Week Eleven</h3><br>
<u>Sprint 4 – Update and Delete, Adding Data, Add to Live server</u>

Working on getting page to reload after Sea Lion has been updated or deleted.




