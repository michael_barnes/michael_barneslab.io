---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Home
order: 0
---
<p class="center_text_big">IN602 Project Portfolio</p>
<p class="center_text">Otago Polytechnic Semester One 2018</p>
<a href="#" class="image centered"><img src="assets/images/sealion.jpg" alt="" /></a>
